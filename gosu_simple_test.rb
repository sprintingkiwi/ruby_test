# Encoding: UTF-8

require 'rubygems'
require 'gosu'

@beep = Gosu::Sample.new("test_sound_1.ogg")
@beep.play
puts "done"

class Tutorial < (Example rescue Gosu::Window)
  def initialize
    super 640, 480
    self.caption = "Tutorial Game"
  end
  
  def update
    if Gosu.button_down? Gosu::KB_UP or Gosu.button_down? Gosu::GP_BUTTON_0
      @player.accelerate
    end
    @player.move
    @player.collect_stars(@stars)
    
    if rand(100) < 4 and @stars.size < 25
      @stars.push(Star.new(@star_anim))
    end
  end
  
  def draw
    @background_image.draw(0, 0, ZOrder::BACKGROUND)
    @player.draw
    @stars.each { |star| star.draw }
    @font.draw("Score: #{@player.score}", 10, 10, ZOrder::UI, 1.0, 1.0, Gosu::Color::YELLOW)
  end
  
  def button_down(id)
    if id == Gosu::KB_ESCAPE
      close
    else
      super
    end
  end
end

Tutorial.new.show if __FILE__ == $0
