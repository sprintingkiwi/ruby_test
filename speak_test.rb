require 'rubysounds'

beep(600, 200)

speak 'hello ruby'

speak 'lets play'


# Guess the number game
speak 'guess the number between 0 and 99'
number = rand(0..99)
answer = gets.chomp.to_i

while answer != number
	speak 'wrong'

	if answer > number
		speak 'too high'
	else
		speak 'too low'
	end
	
	answer = gets.chomp.to_i
end

speak 'Great! It was' + answer.to_s